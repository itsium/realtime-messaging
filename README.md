## Server

```shell
npm install
DEBUG=express:* node server.js
```

## Phonegap

```shell
cd phonegap/
cordova plugin add https://git-wip-us.apache.org/repos/asf/cordova-plugin-media.git
cordova plugin add https://git-wip-us.apache.org/repos/asf/cordova-plugin-file.git
cordova plugin add https://git-wip-us.apache.org/repos/asf/cordova-plugin-file-transfer.git
cordova plugin add https://git-wip-us.apache.org/repos/asf/cordova-plugin-media-capture.git

#cordova platform add ios
#cordova build ios
#cordova run ios

#cordova platform add android
#cordova build android
#cordova run android
```