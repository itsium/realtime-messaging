var broadcast,
    path = require('path'),
    express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server);

io.set('log level', 3);

app.use(express.logger("dev"));
app.use(express.bodyParser({
    uploadDir: __dirname + '/uploads',
    keepExtensions: true
}));
app.use('/uploads', express.static(path.join(__dirname, './uploads')));
app.use(app.router);

// HTTP
app.get('/', function (req, res) {
    res.sendfile(__dirname + '/client.html');
});

app.get('/vendors/socket.io.js', function (req, res) {
    res.sendfile(__dirname + '/vendors/socket.io.js');
});

app.get('/client.js', function (req, res) {
    res.sendfile(__dirname + '/client.js');
});

app.post('/upload', function (req, res) {

    if (!req.files || !req.files.file) {
        res.send('KO');
        return false;
    }

    var file = req.files.file,
        args = {
            file: '/uploads/' + path.basename(file.path)
        };

    chat.emit('audio', args);

    res.send('OK');

});

// Websocket
var chat = io
    .of('/chat')
    .on('connection', function (socket) {

    // socket.emit('a message', {
    //     that: 'only'
    //   , '/chat': 'will get'
    // });
    // chat.emit('a message', {
    //     everyone: 'in'
    //   , '/chat': 'will get'
    // });

        if (!broadcast) {
            broadcast = socket.broadcast;
        }

        socket.on('text', function (args) {
            console.log('I received a text message ', args);
            socket.broadcast.emit('text', args);
        });

        socket.on('audio', function (args) {
            console.log('I received an audio message ', args);
            socket.broadcast.emit('audio', args);
        });

        socket.on('disconnect', function() {
            io.sockets.emit('user disconnected');
        });

    });

server.listen(8080);
