(function() {

    var messages,
        input,
        send,
        chat = io.connect('http://10.10.10.62:8080/chat');

    document.addEventListener('DOMContentLoaded', function() {
        messages = document.getElementById('messages');
        message = document.getElementById('message');
        send = document.getElementById('send');

        function send_msg(msg) {

            send.setAttribute('disabled', 'disabled');
            message.value = '';

            chat.emit('text', {
                msg: msg
            });

            messages.innerHTML += ('me: ' + msg + '<br>');

        }

        message.addEventListener('keypress', function(e) {

            var msg = message.value;

            if (!msg.length) {
                send.setAttribute('disabled', 'disabled');
                return false;
            } else if (e.keyCode === 13) {
                send_msg(msg);
            } else {
                send.removeAttribute('disabled');
            }

        });

        send.addEventListener('click', function() {

            var msg = message.value;

            if (!msg.length) {
                return false;
            }

            send_msg(msg);
        });

    });

    chat.on('connect', function () {
        console.log('connect');
        messages.insertAdjacentHTML('beforeend', 'CONNECTED !<br>');
    });

    chat.on('disconnect', function () {
        console.log('disconnect');
        messages.insertAdjacentHTML('beforeend', 'DISCONNECTED !<br>');
    });

    chat.on('text', function(args) {
        console.log('text: ', args);
        messages.insertAdjacentHTML('beforeend', 'other: ' + args.msg + '<br>');
    });

    chat.on('audio', function(args) {
        console.log('audio: ', args);
        messages.insertAdjacentHTML('beforeend', '<audio src="' + args.file +
            '" controls autoplay></audio><br>');
    });

    window.chat = chat;

}());
