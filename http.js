var broadcast,
    path = require('path'),
    express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server);

app.use(express.logger("dev"));
app.use(express.bodyParser({
    uploadDir: __dirname + '/uploads',
    keepExtensions: true
}));
app.use('/uploads', express.static(path.join(__dirname, './uploads')));
app.use(app.router);

// HTTP
app.get('/', function (req, res) {
    res.sendfile(__dirname + '/client.html');
});

app.get('/vendors/socket.io.js', function (req, res) {
    res.sendfile(__dirname + '/vendors/socket.io.js');
});

app.get('/client.js', function (req, res) {
    res.sendfile(__dirname + '/client.js');
});

app.post('/upload', function (req, res) {

    console.log(req.files);

    if (!req.files || !req.files.file) {
        res.send('KO');
        return false;
    }

    var file = req.files.file;

    broadcast.emit('audio', {
        file: '/uploads/' + path.basename(file.path)
    });

    res.send('OK');

});
