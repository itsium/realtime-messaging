(function() {

    var messages,
        input,
        send,
        stop_audio,
        start_audio,
        fileSystem,
        fileEntry,
        mediaRec,
        mediaPlay,
        recording = false,
        playing = false,
        connected = false,
        ua = navigator.userAgent.toLowerCase(),
        isAndroid = (ua.indexOf("android") > -1),
        soundFile = (isAndroid === true ? 'poc_record.mp3' : 'poc_record.wav'),
        serverURI = 'http://10.10.10.62:8080',
        chat = io.connect(serverURI + '/chat');

    function hasClass(el, cls) {
        return ((' ' + el.className + ' ').indexOf(' ' + cls + ' ') !== -1);
    }

    function addClass(el, cls) {
        if (!hasClass(el, cls)) {
            el.className += ' ' + cls;
            el.className = el.className.replace(/^\s+|\s+$/g, '');
            return true;
        }
        return false;
    }

    function removeClass(el, cls) {
        if (hasClass(el, cls)) {
            var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
            el.className = el.className.replace(reg, ' ').replace(/^\s+|\s+$/g, '');
            return true;
        }
        return false;
    }

    var console = {
        log: function() {
            alert(JSON.stringify(arguments));
        }
    };

    window.onerror = function() {
        console.log(arguments);
    };

    // function captureSuccess(mediaFiles) {
    //     var i, len;
    //     for (i = 0, len = mediaFiles.length; i < len; i += 1) {
    //         uploadFile(mediaFiles[i]);
    //     }
    // }

    // // Called if something bad happens.
    // //
    // function captureError(error) {
    //     var msg = 'An error occurred during capture: ' + error.code;
    //     navigator.notification.alert(msg, null, 'Uh oh!');
    // }

    // // A button will call this function
    // //
    // function captureAudio() {
    //     // Launch device audio recording application,
    //     // allowing user to capture up to 2 audio clips
    //     navigator.device.capture.captureAudio(captureSuccess, captureError, {limit: 2});
    // }

    function captureAudio() {

        console.log('recording: ', recording);

        if (recording === true) {
            return false;
        }

        var src = fileEntry.toURL();//fileEntry.toURI();

        recording = true;
        mediaRec = new Media(src,
        // success callback
        function() {
            console.log("recordAudio():Audio Success", mediaRec);
            uploadFile(fileEntry);
        },

        // error callback
        function(err) {
            console.log("recordAudio():Audio Error: "+ err.code + ' || ' + err.message);
        },

        // status callback
        function() {
            console.log('status change', arguments);
        });

        // Record audio
        mediaRec.startRecord();
        stop_audio.removeAttribute('disabled');
        start_audio.setAttribute('disabled', 'disabled');
        setTimeout(stopCaptureAudio, 20000);
    }

    window.captureAudio = captureAudio;

    function stopCaptureAudio() {
        if (recording === true) {
            recording = false;
            start_audio.removeAttribute('disabled');
            stop_audio.setAttribute('disabled', 'disabled');
            mediaRec.stopRecord();
        }
    }

    window.stopCaptureAudio = stopCaptureAudio;

    function fs_error(err) {
        console.log('Request FS error: ', err);
    }

    function fs_success(fs) {
        fileSystem = fs;
        fs.root.getFile(soundFile, {
            create: true,
            exclusive: false
        },
        file_create_success,
        file_create_error);
    }

    function file_create_success(file_entry) {
        fileEntry = file_entry;
    }

    function file_create_error(err) {
        console.log('File creation error: ', err);
    }

    function create_empty_wave() {
        window.requestFileSystem(LocalFileSystem.TEMPORARY, 0,
            fs_success, fs_error);
    }

    // Upload files to server
    function uploadFile(mediaFile) {

        var ft = new FileTransfer(),
            opts = new FileUploadOptions(),
            path = mediaFile.toURL(),//mediaFile.fullPath,
            name = mediaFile.name;

        opts.fileName = name;
        opts.fileKey = "file";
        opts.chunkedMode = false;

        //console.log(path, name);

        ft.upload(path, (serverURI + '/upload'),
            function(result) {
                //console.log('Upload success: ' + result.responseCode);
                //console.log(result.bytesSent + ' bytes sent');
            },
            function(error) {
                console.log('Error uploading file ' + path + ': ' + error.code);
                console.log(error);
            }, opts);
    }

    document.addEventListener('deviceready', function() {

        messages = document.getElementById('messages');
        message = document.getElementById('message');
        send = document.getElementById('send');
        start_audio = document.getElementById('start_audio');
        stop_audio = document.getElementById('stop_audio');

        if (isAndroid) {
            document.getElementById('style').setAttribute('disabled', 'disabled');
        }

        create_empty_wave();

        function send_msg(msg) {

            send.setAttribute('disabled', 'disabled');
            message.value = '';

            chat.emit('text', {
                msg: msg
            });

            messages.insertAdjacentHTML('beforeend',
                '<div class="message to">' + msg + '</div>');

        }

        message.addEventListener('keypress', function(e) {

            var msg = message.value;

            if (!msg.length) {
                send.setAttribute('disabled', 'disabled');
                return false;
            } else if (e.keyCode === 13) {
                message.blur();
                send_msg(msg);
            } else {
                send.removeAttribute('disabled');
            }

        });

        message.addEventListener('focusin', function(e) {
            addClass(message.parentNode, 'active');
        });

        message.addEventListener('focusout', function(e) {
            if (!message.value.length) {
                removeClass(message.parentNode, 'active');
            }
        });

        send.addEventListener('click', function() {

            var msg = message.value;

            if (!msg.length) {
                return false;
            }

            send_msg(msg);
        });

    });

    function play_audio(el) {

        if (playing === true) {
            mediaPlay.stop();
        }

        var url = el.getAttribute('data-file');

        mediaPlay = new Media(url, function() {
            playing = false;
        }, function() {}, function(status) {
            //console.log('media status', arguments);
            if (status === Media.MEDIA_STOPPED) {
                playing = false;
            } else {
                playing = true;
            }
        });

        playing = true;
        mediaPlay.play();
        mediaPlay.setVolume('1.0');
    }

    window.play_audio = play_audio;

    chat.on('connect', function () {
        if (messages) {
            messages.insertAdjacentHTML('beforeend', 'CONNECTED !<br>');
        }
    });

    chat.on('disconnect', function () {
        if (messages) {
            messages.insertAdjacentHTML('beforeend', 'DISCONNECTED !<br>');
        }
    });

    chat.on('text', function(args) {
        if (messages) {
            messages.insertAdjacentHTML('beforeend',
                '<div class="message from">' + args.msg + '</div>');
        }
    });

    chat.on('audio', function(args) {
        if (messages) {
            messages.insertAdjacentHTML('beforeend',
                '<div class="message from audio" data-file="' +
                serverURI + args.file +
                '" ontouchend="javascript:play_audio(this);">audio</div>');
        }
    });

    window.chat = chat;

}());
